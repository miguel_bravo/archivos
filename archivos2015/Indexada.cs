﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace archivos2015
{
    public partial class Indexada : Form
    {
        Diccionario diccionario;
        List<string> dats;
        bool DelD,modD;
        List<Indices> tablaIndices;
        OrgIndex indexada;

        public Indexada()
        {
            InitializeComponent();
            dats = new List<string>();
            DelD = false;
            modD = false;

        }

        public Indexada(Diccionario dicc)
        {
            InitializeComponent();
            diccionario = dicc;
            loadComboEnts();
            dats = new List<string>();
            DelD = false;
            modD = false;
            tablaIndices = new List<Indices>();
            string nombre = dicc.NomDic.Substring(0, dicc.NomDic.Length-3);
            indexada = new OrgIndex(nombre);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Entidad ent = diccionario.getEntByName(comboBox1.Text);


        }

        public void llenaDataTabIndex()
        {

        }

        private void loadComboEnts()
        {
            foreach (Entidad i in diccionario.Entidades)
                comboBox1.Items.Add(i.Nombre);
        }
    }
}
